
local two_pi = math.pi * 2.0
local max = math.max
local floor = math.floor
local cos = math.cos
local sin = math.sin

---Return index of first occurance of an item in an integer-indexed table
---@param searchTable table
---@param toFind any
---@return integer?
function Index(searchTable, toFind)
	for i=1,#searchTable do
		if searchTable[i] == toFind then
			return i
		end
	end
	return nil
end

---Returns the other team's index from TEAMS table 
---@param teamIndex 1|2
---@return integer
function EnemyTeamIndex(teamIndex)
	return ((teamIndex-1)+1)%2 + 1
end

---Calculates a new position by moving towards an angle by distance
---@param pos Position
---@param towards number
---@param distance number
---@return Position
function VTowardsAngle(pos, towards, distance)
	return World_Pos(pos.x + distance * cos(towards), pos.y, pos.z + distance * sin(towards))
end

---Reticules can be found in attrib\UI\reticule. Some may cause crashes.
---@param pos Position
---@param radius number
---@param reticule? string | PBG
---@param reticuleDistance? number
---@param reticuleRadius? number
---@return ReticuleID[]
function UI_CreateReticuleCircle(pos, radius, reticule, reticuleDistance, reticuleRadius)
	if not reticule then reticule = 'towing_detach_target_optional_direction' end
	if not reticuleDistance then reticuleDistance = 1.0 end
	if not reticuleRadius then reticuleRadius = 0.225 end

	if scartype(reticule) == ST_STRING then
		---@cast reticule string
		reticule = BP_GetReticuleBlueprint(reticule)
	end
	---@cast reticule PBG

	local circumference = two_pi * radius
	local count = max(1, floor(circumference / reticuleDistance))

	local ids = {}
	for angle=0,two_pi,two_pi / count do
		ids[#ids + 1] = UI_CreateReticule(VTowardsAngle(pos, angle, radius), reticuleRadius, reticule)
	end

	return ids
end
