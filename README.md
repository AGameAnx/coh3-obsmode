# COH3-OBSMode

Testing setup and code repository for the OBSMode mods for Company of Heroes 3.

Main useful script files from the project:
- `assets/scenarios/OBSTest/importer.scar`
- `assets/scenarios/OBSTest/utils.scar`
- `assets/scenarios/OBSTest/obs.scar`
